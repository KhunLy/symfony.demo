<?php


namespace App\Entity;


class Message
{
    /**
     * @var string
     */
    private $author;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $object;

    /**
     * @var string
     */
    private $content;

    /**
     * @return string
     */
    public function getAuthor(): string
    {
        return $this->author;
    }

    /**
     * @param string $author
     * @return Message
     */
    public function setAuthor(string $author): Message
    {
        $this->author = $author;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return Message
     */
    public function setEmail(string $email): Message
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getObject(): string
    {
        return $this->object;
    }

    /**
     * @param string $object
     * @return Message
     */
    public function setObject(string $object): Message
    {
        $this->object = $object;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Message
     */
    public function setContent(string $content): Message
    {
        $this->content = $content;
        return $this;
    }


}