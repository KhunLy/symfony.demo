<?php

namespace App\Controller;


use App\Entity\Message;
use DateTime;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    /**
     * @Route(name="default_home", path="/")
     */
    public function home()
    {
        return $this->render('default/home.html.twig', [
            'nomDeMaVariable' => 'Khun',
            'variableNumerique' => 42.42,
            'variableDate' => new DateTime(),
            'variableNonEchappee' => '<a href="http://google.be">lien</a>'
        ]);
    }

    /**
     * @Route(name="default_contact", path="/contact")
     */
    public function contact(Request $req, \Swift_Mailer $mailer) {
        $method = $req->getMethod();
        if($method === 'POST'){
            // récuperer les données du formulaire
            $email = $req->request->get('email');
            $author = $req->request->get('author');
            $object = $req->request->get('object');
            $content = $req->request->get('content');
            // je crée grâce à ces infos un objet "Message"
            $message = new Message();
            $message->setAuthor($author)
                ->setEmail($email)
                ->setObject($object)
                ->setContent($content);

            // envoyer un email
            $mail = new \Swift_Message();
            $mail->setTo('tfwebapp2020@gmail.com');
            $mail->setFrom($message->getEmail());
            $mail->setBody($message->getContent());
            $mail->setSubject($message->getObject());

            $mailer->send($mail);


            // message de confirmation
            // revenir sur la page d'accueil

        }
        return $this->render('default/contact.html.twig');
    }
}